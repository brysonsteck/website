---
title: "Licenses + Warranty Disclaimer"
date: 2022-12-27T13:36:21-07:00
draft: false
---
This website is made available unto you under dual licenses.

The first license, the Creative Commons Attribution-ShareAlike 4.0 license (CA-BY-SA 4.0), covers all the website's **content**, including all Markdown files used to make the blog posts and project pages.

The second license, the BSD 2-Clause license, covers everything else that is not the website's content, including the **HTML and CSS** used to generate the site. 

The source files/code mentioned above is available under the respective licenses on [this Codeberg repository](https://codeberg.org/brysonsteck/brysonsteck.xyz).

## TL;DR

The following are summaries of the licenses used for this website and ***are NOT a substitute*** for the actual licenses, either created by the license creator themselves or by my interpretation.

* [Creative Commons Attribution-ShareAlike 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode) (aka CA-BY-SA 4.0)
  * You are free to:
    * **Share:** Copy and redistribute the material in any medium or format. 
    * **Adapt:** Remix, transform, and build upon the material for any purpose, even commercially. 
  * Under the following terms:
    * **Attribution:** You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
    * **ShareAlike:** If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
    * **No additional restrictions:** You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits. 
  * Keeping in mind:
    * You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation. 
    * No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material. 
* [BSD 2-Clause License](#bsd-2-clause-license)
  * You are free to:
    * Use the software **commercially or privately.**
    * **Modify and distribute** the software.
  * Under the following terms:
    * Reproduce **the same copyright notice, list of conditions, and disclaimer as shown [here](#bsd-2-clause-license),** in the source material and (if applicable) any binary form containing/distributing the software via documentation or other means.
  * Keeping in mind:
    * The copyright holders **do not distribute a warranty with the software, nor are they liable for any damages.**

### CA-BY-SA 4.0

Please read the full license at [https://creativecommons.org/licenses/by-sa/4.0/legalcode](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

### BSD 2-Clause License

```
Copyright (c) 2022-2023, Bryson Steck

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```
