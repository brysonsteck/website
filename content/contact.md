---
title: "Contact Me!"
date: 2022-12-27T18:41:02-07:00
draft: false
---
## Me on the Interwebs

* Codeberg: [@brysonsteck](https://codeberg.org/brysonsteck)
* Self-hosted Git Server: [https://git.brysonsteck.xyz](https://git.brysonsteck.xyz)
* Email: [me@brysonsteck.xyz](mailto:me@brysonsteck.xyz)
* PGP Public Key: [A0858B7204DC5DDD](/pgpkey.txt)

Please use my PGP key if possible and I'll use yours. Although I will read your email regardless.

## Job Opportunities

**Employment Status:** Currently employed full-time.

If you would like to extend a job opportunity, please email me! I probably won't respond if I *really* like my current job, but I would like to hear it either way.
