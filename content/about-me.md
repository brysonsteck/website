---
title: "About Me"
date: 2022-12-26T22:54:07-07:00
draft: false
---
![](/bryson-smol.png)

Hey there! My name is Bryson Steck. I live in Utah and I'm currently a graduate student getting my Masters in Computer Science. Some people notably find me funny, easy to work with, and very addicted to dill pickles. 

My interests include everything Linux, system and network administration, privacy and security, and figuring out how the hell I got that thundercloud that one time playing in a Mario Kart Wii Mogi.

# Education

**Bachelors of Science, Computer Science** - Utah State University, graduated May 2023
**Masters of Science, Computer Science** - Utah State University, graduating May 2025

# Skills

* C/C++
* Vim and VSCode([ium](https://vscodium.com/))
* Linux, BSD, and other UNIX-like OSes
* Perl, Bash, and POSIX shell scripting
* Java, Kotlin, and Android development
* Ansible, Cron automations
* Spigot Minecraft plugin development
* System administration for Windows/Linux
* Organization
* Python
* HTML/CSS
* JavaScript, TypeScript, *Script
* React
* Node.JS

# My Tech Stack

* Framework Laptop 13
  * Intel Core i7-1370P
  * 32GB of RAM
  * 1TB NVME SSD
  * 1 USB-C, 1 HDMI, 2 USB-A Cards
  * Gentoo Linux
  * Xmonad
* Custom-built Desktop
  * AMD Ryzen 5900X
  * 64GB of RAM
  * 1TB (boot) and 2TB (data) NVME SSDs
  * AMD Radeon RX 6700 XT
  * MSI B550-A PRO
  * NZXT H5 Flow
  * Arch Linux
  * KDE
* Google Pixel 8 Pro
  * Google Tensor 3
  * 12GB of RAM
  * 256GB of storage
  * GrapheneOS

I recently upgraded to the Framework Laptop, and I am in love with this thing! I have had issues with FedEx shipping it though, they seemed to spike my package playing volleyball or something since my keyboard arrived dented. Thankfully, Framework support was very helpful in getting my damaged parts replaced at no additional charge, including covering the shipping fees for the return of the damaged part.

I also completely overhauled my existing desktop. I wanted something with a little more horsepower for games that I play and just for future proofing as well. My trusty Dell Optiplex was fantastic but started to fall behind, but 'tis not the end for my valiant steed. I hold it aside to hopefully start using as a air-gapped machine to learn how such systems are maintained and use it's optical drive for ripping DVDs and (eventually) Blu-Rays. I still recommend used Optiplex systems for those needing a cheap Linux computer though. You can read more below.

## Old Tech Stack (may they rest in peace)

* Acer Swift SF314-42
  * AMD Ryzen 7 4700U
  * 8GB of RAM
  * 512GB NVME SSD
  * Gentoo Linux
  * [dwm](/projects/dwm)
* Dell Optiplex 7020
  * Intel Core i5-4590
  * 16GB of RAM
  * 512GB SATA SSD, 2TB Hard Drive
  * AMD RX 580
  * Arch Linux
  * KDE
* Google Pixel 5a
  * Qualcomm LITO
  * 6GB of RAM
  * 128GB of storage
  * GrapheneOS

While I'm settled with it, I don't recommend you buy an Acer Swift SF314-42. It's an okay price for what you get at around $700, ***BUT*** the RAM is soldered, meaning no-go for any future upgrades, the screen's bevel is bad and falls apart from heat if you use it at a dock like I do, and it's keyboard is lackluster with an odd selection of media keys and no Linux support for it's fingerprint scanner. Obviously the fingerprint scanner doesn't matter much to me nowadays, but for the DE guys it may be a deal-breaker. I recently upgraded to the Framework Laptop 13 since this laptop was no longer suiting my needs both power-wise and RAM-wise.

If you want a cheap desktop computer that does amazing under Linux, get a used Dell Optiplex or another old office PC. I got mine for $50, people often sell them for under $200 complete, and they are easily upgradable with about 90% standard parts. The only issues I have with them is the lack of space for storage and a front case fan and their garbage proprietary 8-pin motherboard power cable. Both the issues with the fan and storage can be mitigated if you are clever enough, but the 8-pin seems to make the processor inside run at a fraction of it's actual potential.

My first Android phone was a Google Pixel 5a, and depending on who you ask it's either a downgrade or an upgrade. I was originally an Apple guy through and through, until I felt their advertising about their privacy became hypocritical and I became more weary about the privacy issues we have on the internet.
