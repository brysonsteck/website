---
title: "brysonsteck.xyz"
description: "Hello there!"
icon: "brysonsteck.xyz.jpg"
repo: "https://codeberg.org/brysonsteck/website"
license: "BSD 2-Clause License and CC BY-SA 4.0"
languages: "Markdown, HTML, CSS, and JavaScript"
wip: false
current: true
date: 2022-12-02T12:05:39-07:00
enddate: "January 2, 2006"
draft: false
---
You're here! This website is a static site generated with [Hugo](https://gohugo.io) and hosted with Nginx, and replaces the [Django website](/projects/django-website) I was using for the second half of 2022. 

## Fonts

* **Inter**
* `JetBrains Mono`

## Colorscheme

* Gruvbox

All content on this site (i.e. all Markdown files) is available and distributed under the Creative Commons Attribution-ShareAlike license version 4.0, or CC BY-SA 4.0 for short. All other code (i.e. all HTML/CSS that generates the site) is available and distributed under the BSD-2 Clause License.

You can read each of these licenses in full detail [here](/licenses).
