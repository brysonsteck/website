---
title: "ServerCraft"
description: "A graphical interface for managing Minecraft servers"
icon: "ServerCraft.png"
repo: "https://codeberg.org/brysonsteck/ServerCraft"
license: "GNU General Public License v3.0"
languages: "Kotlin and Gradle"
wip: false
current: true
date: 2023-05-27
enddate: "January 2, 2006"
draft: false
---

ServerCraft is a GUI program created with JavaFX in order to help technically-unskilled gamers host their own Minecraft servers for their friends by automatically downloading the correct version of Java and building the server with Spigot's BuildTools. This project is in it's early infancy, but is currently usable for those who just need a simple Minecraft server.

## Features

* Common settings available at a glance
* Separate section for common settings that are a little more advanced, such as:
  * The amount of RAM to allocate to the server
  * Render and simulation distances
  * Enabling command blocks

## Planned Features

* "Advanced" configuration mode for GUI nuts
* Automatic port forwarding using UPnP
* Ability to manage Spigot plugins installed in a server directory
* Create any Minecraft server >= version 1.8

## Download

ServerCraft is available as binaries and installers, packaged with OpenJDK 17 and the ServerCraft jar, for Windows, Linux, and macOS [here](https://codeberg.org/brysonsteck/ServerCraft/releases). Simply download the correct file for your operating system. (`.exe` or `.msi` for Windows, `.dmg` or `.pkg` for macOS, etc.) Jar files are also available for the respective operating systems as compressed archives. These Jar files are *not* cross-platform, due to how JavaFX renders the windows on each system.

You can also download the source code and compile it yourself. ServerCraft uses the Gradle build system and requires Java 11 or later due to JavaFX requirements. Instructions to do so are available on the project's repository hosted on Codeberg. (See top of page.)
