---
title: "Wiimmfi Watcher"
description: "An Android application for that special Mario Kart Wii loser"
icon: "wiimmfi-watcher.png"
license: "GNU General Public License v3.0"
languages: "Java"
current: false
date: 2021-04-30
enddate: "September 14, 2021" 
draft: false
---
Wiimmfi Watcher is an unofficial Android app that displays online Mario Kart Wii room data by entering a friend code. Wiimmfi is a fan-made server that replaces the now defunct Nintendo Wi-Fi Connection ("Nintendo WFC" for short) and supports many popular Nintendo Wii and DS games, with the most popular being Mario Kart Wii. I am not affiliated with the Wiimmfi project nor it's developers. This app can be used in several different languages.

Originally a school project, I saw a use for it in the real world since the Wiimmfi website doesn't have a mobile friendly view. This application is also my first piece of software released to the public and it remains special in my heart. It is available on the Google Play Store here, with it's source code available on GitHub under the GPL-3.0 license, for Android devices running KitKat 4.4 or newer.

## ⚠️ DEPRECATED ⚠️

Wiimmfi Watcher is no longer maintained for reasons beyond my control. Due to the Wiimmfi website implementing DDoS protection as a result of recent attacks on their servers since September 4th, 2021, Wiimmfi Watcher is unable to access Wiimmfi servers and all requests the app makes returns `HTML Error 503: Service Unavailable`.

### Download

You can visit the Google Play Store to [download](https://play.google.com/store/apps/details?id=me.brysonsteck.wiimmfiwatcher) the most recent and stable version, or you can [clone the repository](https://github.com/brysonsteck/wiimmfi-watcher) if you would like to build it from scratch.
