---
title: "dwm"
description: "My personal fork of suckless' Dynamic Window Manager"
icon: "dwm.png"
repo: "https://codeberg.org/brysonsteck/dwm"
license: "MIT/X Consortium License"
languages: "C"
wip: false
current: true
date: 2021-10-08T12:05:56-07:00
enddate: "January 2, 2006"
draft: false
---
This is my personal fork of the [suckless tool](https://dwm.suckless.org), Dynamic Window Manager (`dwm`), that I use on any computers where workflow is deemed critical. 

## My changes/patches

Here are the biggest changes and patches I applied to this fork of `dwm`:

* Gruvbox Dark themed
* Uses JetBrains Mono as the font
* `MODKEY` set to Super
* Added the following patches from the suckless website:
    * swallow
    * cursorwarp
    * fullgaps
    * hide_vacant_tags
    * sticky and stickyindicator
    * bottomstack
    * there may be more... I have no idea lol
* A couple keybinds are not stock (compared to vanilla dwm):
    * Quitting dwm rebinded to `Super+Shift+BackSpace` and confirms quitting with [a script](https://codeberg.org/brysonsteck/dotfiles/src/branch/master/x/quitconf)
    * The increments to resize the master area have been decreased
    * Launching `dmenu` launches a binary called `dmenu_run_history` which I found online somewhere, literally no idea where I found it lol. Eventually I'll patch it into the window manager code instead someday
    * Launching the terminal is bound to [a script](https://codeberg.org/brysonsteck/dotfiles/src/branch/master/x/spawn-alacritty.sh) that launches Alacritty in the same directory as the current terminal window

You can find more information about how to install and the license information on the repository.
