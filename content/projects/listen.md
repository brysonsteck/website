---
title: "Listen"
description: "A simple automation tool for literally anything you use a shell for"
icon: "listen.png"
repo: "https://codeberg.org/brysonsteck/listen"
license: "GNU General Public License v3.0 ONLY"
languages: "Perl"
wip: false
current: true
date: 2022-09-07T12:06:25-07:00
enddate: "January 2, 2006"
draft: false
---
A simple automation program written in Perl that runs shell command(s) when the specified file(s) are modified.

## Why Use Listen?

* Don't switch between terminals and run that command every time! Let Listen do it for you.
* Very useful for singular files or small projects.
  * For example, you can automatically compile a C++ file with `g++` anytime you make a change to that source file.
  * Same goes for simple Python scripts, Bash scripts, etc.
* Great for students who work on a small set of source files for assignments.

## Other Features

* Run the command when any or all files specified are modified.
* Run the command before Listen starts waiting for modifications.
* Runs out of the box! Perl is installed on any modern UNIX-like operating system.
* Use `/bin/cksum` to determine file modification instead of the time it was written to.
  * Could be useful for binaries.
  * Also distributed with Unix-like OSes, such as Linux, BSD, and macOS. No configuration required between OSes.
  * However, if you use Windows (like a weirdo), you must use `certutils` or a third-party tool by modifying the script.
* Timeout between modifications 
  * Helpful if linters are involved, such as `clang-format`, to avoid multiple runs of a command.
* Free and Open Source software!

