---
title: "My dotfiles"
description: "The cool scripts that make my Linux boys go *zoom*"
icon: "shells.png"
repo: "https://codeberg.org/brysonsteck/dotfiles"
license: "Unlicense"
languages: "Bash, POSIX Shell, and Perl"
wip: false
current: true
date: 2021-10-09T12:05:46-07:00
enddate: "January 2, 2006"
draft: false
---
My collection of scripts and configuration files that help make me more efficient and/or make things look snazzy. This includes the following files:

* Alacritty terminal config
* Bash profile
* Dunst config
* Fonts that I use
* Htop config
* Tmux config
* Vim configs, themes and scripts
* VSCodium/VSCode settings
* Xinit scripts
    * Along with helper scripts mentioned inside them

The repository also contains an update script so that copies of all the dotfiles are stored safely in case of any mistakes and means linking isn't necessary (because that has *definetly* not happened to me before).
