---
title: "Django Blog"
description: "The old Django blog that powered the long-forgotten brysonsteck.xyz"
icon: "tommy.jpg"
repo: "https://codeberg.org/brysonsteck/django-blog"
license: "GNU Affero General Public License v3.0"
languages: "Python, HTML, CSS, and JavaScript"
wip: false
current: false
date: 2022-02-27T12:06:14-07:00
enddate: "December 2, 2022"
draft: false
---
![](/old-website.png)

This was the old Django project that originally powered brysonsteck.xyz with Gunicorn before I realized three things:

1. I hate Python.
1. The design path I chose was pretty bulky; it took up a lot of unnecessary space.
1. The way I was creating the blogs and project pages made a static site generator look more appealing.

As a result, the [new static website](/projects/this-website) was born with Hugo!

### Features

* Automated install and database migration with `install.py`
* Creation of blogs and project pages with the [django-markdownfield](https://pypi.org/project/django-markdownfield/) package
* Cool FontAwesome icons
* Mobile friendliness with Bootstrap CSS
* Gruvbox sweetness

