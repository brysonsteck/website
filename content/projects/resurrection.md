---
title: "Resurrection"
description: "A Spigot Minecraft plugin that forces players to wait absurd amounts of time before respawning"
icon: "resurrection.png"
license: "GNU Affero General Public License v3.0"
languages: "Java"
current: true
date: 2021-06-06
enddate: "n/a"
draft: false
---
Resurrection is a Spigot/Bukkit Minecraft Server plugin that forces players to wait a certain amount of time before rejoining the world. This allows for tactical planning for games such as faction survival and other PvP gamemodes as it can severely penalize an entire team if care is not taken.

Resurrection is intended to make players wait long amounts of time between death and respawn, preferably 24 hours minimum. Resurrection sets the time to be 24 hours by default. However, you can make players to wait any amount of time you wish, whether shorter or longer than the default 24 hours, but your experience with this plugin may be impacted.

![](/resurrection-screenshot.png)

## Compatibility 

Tested Minecraft Versions: 1.8<sup>*</sup>, 1.9, 1.10, 1.12, 1.13, 1.14, 1.15, 1.16, 1.17, 1.18, 1.19

Native Version: 1.16

Resurrection is only confirmed to run on vanilla Spigot or Bukkit servers, meaning you built the server yourself using `BuildTools` with no extra settings, or downloaded it from official sources such as their website. Resurrection is **NOT GUARANTEED** to run on **ANY** fork of Spigot/Bukkit servers, such as Tuinity or Paper. Issues reported involving these forks may not be provided a solution unless proven that the issue still happens on vanilla versions.

<sup>*</sup>For Minecraft Servers version 1.8, you must download a special build of Resurrection in order for the plugin to work properly.

## Commands

* `/about`
    * Displays information about the plugin, including links to download, and also warns the command-runner if the plugin is outdated. 
* `/bug`
    * Displays contact information in case a bug occurs, such as links to the GitHub issues page and the Google Form.
* `/dead`
    * Displays all the players currently awaiting resurrection and how long they have left. 
* `/howlong [PLAYER]`
    * Shows the player how long they (or the specified player) have until they are resurrected.
    * This command requires a player to be specified when ran from the console. 
* `/resurrect PLAYER`
    * Manually resurrects a player if they are dead.
    * Operator-only command. 
* `/source`
    * Informs the user that this plugin is free and open source under the [GNU Affero General Public License Version 3.](https://github.com/brysonsteck/resurrection/blob/master/LICENSE)

### More Information

To find more information about Resurrection, including how to submit bugs or learn what files the plugin auto-creates, you can either visit the [Github repository](https://github.com/brysonsteck/resurrection), which contains all the source code, released .Jar files and the home for all bug reports, or by visiting the [Spigot](https://www.spigotmc.org/resources/resurrection.94542/) and [Bukkit](https://dev.bukkit.org/projects/resurrection-official) resource pages where you can mainly leave reviews and easily see version history.

Resurrection, it's source code and it's Jar files are licensed under the GNU Affero General Public License. You can read the full license and copyright notice on the Github repository [here](https://github.com/brysonsteck/resurrection/blob/master/LICENSE).

Resurrection currently has 400+ confirmed downloads across Github, Spigot and Bukkit since the first beta released in July 2021 and currently has a 5 star rating on Spigot!
