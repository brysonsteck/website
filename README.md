# Bryson Steck's Website

A simple blog and project showcase website built with Hugo.

Hosted at [brysonsteck.xyz](https://brysonsteck.xyz) (or [here](http://pjovpqngygjscmvhqxlj7wcnjmgkpaepty7gpmnl2z2fix5c7rwfatyd.onion/) if you're based)
