var colon = ":";
function updateTime(){
  var date = new Date()
  var hours = date.getHours()
  var minutes = date.getMinutes()

  if (hours > 11)
    var tod = " pm";
  else
    var tod = " am";

  if (hours > 12)
    hours = hours - 12
  else if (hours == 0)
    hours = 12

  if (minutes < 10)
    minutes = "0" + minutes

  if (colon === ":") 
    colon = " ";
  else if (colon === " ")
    colon = ":";

  var t_str = hours + colon + minutes + tod;
  document.getElementById('time').innerHTML = t_str;
}
setInterval(updateTime, 500);
